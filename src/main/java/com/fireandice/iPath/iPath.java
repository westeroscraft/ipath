package com.fireandice.iPath;
 
import java.util.BitSet;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockCanBuildEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.plugin.java.JavaPlugin;
 
public class iPath extends JavaPlugin implements Listener
{
    private BitSet stop_form = new BitSet();
    private BitSet stop_snow_form_on = new BitSet();
    private BitSet stop_grow = new BitSet();
    private BitSet stop_fade = new BitSet();
    private BitSet stop_spread = new BitSet();
    private BitSet stop_physics = new BitSet();
    private BitSet allow_sugar_cane_place_on = new BitSet();
    private int max_wheat_size;
    private int max_carrot_size;
    private int max_potato_size;
    private boolean verbose;
    
    private static void processList(BitSet set, List<Integer> lst) {
        set.clear();
        if (lst != null) {
            for (Integer v : lst) {
                set.set(v.intValue());
            }
        }
    }
    
    public void onEnable()
    {
        // Get configuration
        FileConfiguration cfg = this.getConfig();
        cfg.options().copyDefaults(true);
        this.saveConfig();
        // Process configuration
        //  stop-grow: list of block IDs to block growth (plants)
        List<Integer> lst = cfg.getIntegerList("stop-grow");
        processList(stop_grow, lst);
        // stop-form: list of block IDs to stop block forming (snow, ice)
        lst = cfg.getIntegerList("stop-form");
        processList(stop_form, lst);
        // stop-spread: list of block IDs to stop block spreading (grass, vines, mushrooms, fire, mycel)
        lst = cfg.getIntegerList("stop-spread");
        processList(stop_spread, lst);
        // stop-snow-form-on: list of block IDs to stop snow forming upon
        lst = cfg.getIntegerList("stop-snow-form-on");
        processList(stop_snow_form_on, lst);
        // stop-physics: list of block IDs to stop physics
        lst = cfg.getIntegerList("stop-physics");
        processList(stop_physics, lst);
        // stop-fade: list of block IDs to stop BlockFade
        lst = cfg.getIntegerList("stop-fade");
        processList(stop_fade, lst);
        // allow-sugar-cane-place-on: list of block IDs to allow sugar cane to be placed upon
        lst = cfg.getIntegerList("allow-sugar-cane-place-on");
        processList(allow_sugar_cane_place_on, lst);
        // max-wheat-grow-size: set maximum growth size for wheat
        max_wheat_size = cfg.getInt("max-wheat-grow-size", -1);
        // max-carrot-grow-size: set maximum growth size for carrots
        max_carrot_size = cfg.getInt("max-carrot-grow-size", -1);
        // max-potato-grow-size: set maximum growth size for potatoes
        max_potato_size = cfg.getInt("max-potato-grow-size", -1);
        
        verbose = cfg.getBoolean("verbose", false);

        getServer().getPluginManager().registerEvents(this, this);
        getLogger().info("iPath is now Enabled, made for and by www.westeroscraft.com!");
     }

    public void onDisable() {
        getLogger().info("iPath disabled.");
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String cmdid, String[] args) {
        if (cmdid.equals("tileentitypopulation")) {
            EnumMap<Material, Integer> cnts = new EnumMap<Material, Integer>(Material.class);
            for (World w : getServer().getWorlds()) {
                int cnt = 0;
                cnts.clear();
                for (Chunk c : w.getLoadedChunks()) {
                    cnt++;
                    for (BlockState bs : c.getTileEntities()) {
                        Material m = bs.getType();
                        if (m == null) continue;
                        Integer bcnt = cnts.get(m);
                        if (bcnt == null) {
                            bcnt = 1;
                        }
                        else {
                            bcnt = bcnt + 1;
                        }
                        cnts.put(m,  bcnt);
                    }
                }
                sender.sendMessage("World: " + w.getName() + ", chunks loaded:" + cnt);
                String msg = "";
                cnt = 0;
                for (Material m : cnts.keySet()) {
                    msg += " " + m.name() + "=" + cnts.get(m);
                    cnt++;
                    if (cnt == 5) {
                        sender.sendMessage(msg);
                        msg = "";
                        cnt = 0;
                    }
                }
                if (cnt != 0) {
                    sender.sendMessage(msg);
                }
            }
        }
        else if (cmdid.equals("tileentityhotspots")) {
            HashMap<String, Integer> cnts = new HashMap<String, Integer>();
            for (World w : getServer().getWorlds()) {
                int cnt = 0;
                int max = 0;
                cnts.clear();
                for (Chunk c : w.getLoadedChunks()) {
                    cnt++;
                    int num = c.getTileEntities().length;
                    if (num == 0) continue;
                    String cid = c.getWorld().getName() + ":" + c.getX() + "," + c.getZ();
                    cnts.put(cid, num);
                    if (max < num) max = num;
                }
                sender.sendMessage("World: " + w.getName() + ", chunks loaded:" + cnt + ", highest tile entity count: " + max);
                sender.sendMessage(" Tiles above 10% of maximum:");
                String msg = "";
                cnt = 0;
                max = (max + 9) / 10;
                for (String cid : cnts.keySet()) {
                    int bcnt = cnts.get(cid);
                    if (bcnt < max) continue; 
                    msg += " " + cid + "=" + cnts.get(cid);
                    cnt++;
                    if (cnt == 5) {
                        sender.sendMessage(msg);
                        msg = "";
                        cnt = 0;
                    }
                }
                if (cnt != 0) {
                    sender.sendMessage(msg);
                }
            }
        }
        else if (cmdid.equals("entitypopulation")) {
            EnumMap<EntityType, Integer> cnts = new EnumMap<EntityType, Integer>(EntityType.class);
            for (World w : getServer().getWorlds()) {
                int cnt = 0;
                cnts.clear();
                for (Entity ent : w.getEntities()) {
                    EntityType et = ent.getType();
                    if (et == null) continue;
                    Integer bcnt = cnts.get(et);
                    if (bcnt == null) {
                        bcnt = 1;
                    }
                    else {
                        bcnt = bcnt + 1;
                    }
                    cnts.put(et,  bcnt);
                }
                sender.sendMessage("World: " + w.getName());
                String msg = "";
                cnt = 0;
                for (EntityType et : cnts.keySet()) {
                    msg += " " + et.name() + "=" + cnts.get(et);
                    cnt++;
                    if (cnt == 5) {
                        sender.sendMessage(msg);
                        msg = "";
                        cnt = 0;
                    }
                }
                if (cnt != 0) {
                    sender.sendMessage(msg);
                }
            }
        }
        return true;
    }
 
    @EventHandler(priority=EventPriority.HIGH, ignoreCancelled=true)
    public void blockForm(BlockFormEvent event)
    {
        BlockState newstate = event.getNewState();
        int newid = newstate.getTypeId();
        if (stop_form.get(newid)) { // If block ID is one we're stopping
            event.setCancelled(true);
            if (verbose)
                getLogger().info(String.format("blockForm(%s:%d,%d,%d) %s cancelled", newstate.getWorld().getName(), newstate.getX(), newstate.getY(), newstate.getZ(), newstate.getType().name()));
        }
        else if (newid == Material.SNOW.getId()) {
            int below = newstate.getBlock().getRelative(BlockFace.DOWN).getTypeId();
            if (stop_snow_form_on.get(below)) {
                event.setCancelled(true);
                if (verbose)
                    getLogger().info(String.format("blockForm(%s:%d,%d,%d) SNOW on %s cancelled", newstate.getWorld().getName(), newstate.getX(), newstate.getY(), newstate.getZ(), Material.getMaterial(below).name()));
            }
        }
    }
    
    @EventHandler(priority=EventPriority.HIGH, ignoreCancelled=true)
    public void blockGrow(BlockGrowEvent event) {
        BlockState newstate = event.getNewState();
        int newid = newstate.getTypeId();
        
        if (stop_grow.get(newid)) { // If block ID is one we're stopping
            event.setCancelled(true);
            if (verbose)
                getLogger().info(String.format("blockGrow(%s:%d,%d,%d) %s cancelled", newstate.getWorld().getName(), newstate.getX(), newstate.getY(), newstate.getZ(), newstate.getType().name()));
        }
        else {
            if ((max_wheat_size >= 0) && (newid == Material.CROPS.getId())) {
                if (newstate.getRawData() > (byte) max_wheat_size) {
                    event.setCancelled(true);
                    if (verbose)
                        getLogger().info(String.format("blockGrow(%s:%d,%d,%d) WHEAT:%d cancelled", newstate.getWorld().getName(), newstate.getX(), newstate.getY(), newstate.getZ(), (int)newstate.getRawData()));
                }
            }
            else if ((max_carrot_size >= 0) && (newid == Material.CARROT.getId())) {
                if (newstate.getRawData() > (byte) max_carrot_size) {
                    event.setCancelled(true);
                    if (verbose)
                        getLogger().info(String.format("blockGrow(%s:%d,%d,%d) CARROT:%d cancelled", newstate.getWorld().getName(), newstate.getX(), newstate.getY(), newstate.getZ(), (int)newstate.getRawData()));
                }
            }
            else if ((max_potato_size >= 0) && (newid == Material.POTATO.getId())) {
                if (newstate.getRawData() > (byte) max_potato_size) {
                    event.setCancelled(true);
                    if (verbose)
                        getLogger().info(String.format("blockGrow(%s:%d,%d,%d) POTATO:%d cancelled", newstate.getWorld().getName(), newstate.getX(), newstate.getY(), newstate.getZ(), (int)newstate.getRawData()));
                }
            }
        }
    }
    
    @EventHandler(priority=EventPriority.NORMAL, ignoreCancelled=true)
    public void blockSpread(BlockSpreadEvent event) {
        BlockState newstate = event.getNewState();
        int newid = newstate.getTypeId();
        
        if (stop_spread.get(newid)) { // If block ID is one we're stopping
            event.setCancelled(true);
            if (verbose)
                getLogger().info(String.format("blockSpread(%s:%d,%d,%d) %s cancelled", newstate.getWorld().getName(), newstate.getX(), newstate.getY(), newstate.getZ(), newstate.getType().name()));
        }
    }

    @EventHandler(priority=EventPriority.NORMAL, ignoreCancelled=true)
    public void blockFade(BlockFadeEvent event) {
        Block blk = event.getBlock();
        int oldid = blk.getTypeId();
        
        if (stop_fade.get(oldid)) { // If block ID is one we're stopping
            event.setCancelled(true);
            if (verbose)
                getLogger().info(String.format("blockFade(%s:%d,%d,%d) %s cancelled", blk.getWorld().getName(), blk.getX(), blk.getY(), blk.getZ(), blk.getType().name()));
        }
    }

    @EventHandler(priority=EventPriority.NORMAL, ignoreCancelled=true)
    public void blockPhysics(BlockPhysicsEvent event) {
        Block block = event.getBlock();
        int newid = block.getTypeId();
        
        if (stop_physics.get(newid)) { // If block ID is one we're stopping
            event.setCancelled(true);
            if (verbose)
                getLogger().info(String.format("blockPhysics(%s:%d,%d,%d) %s cancelled", block.getWorld().getName(), block.getX(), block.getY(), block.getZ(), block.getType().name()));
        }
    }

    @EventHandler(priority=EventPriority.NORMAL, ignoreCancelled=true)
    public void blockCanBuild(BlockCanBuildEvent event) {
        int id = event.getMaterialId();
        if (id == Material.SUGAR_CANE_BLOCK.getId()) {
            Block b = event.getBlock();
            Block bbelow = b.getRelative(BlockFace.DOWN);
            int below = bbelow.getTypeId();
            event.setBuildable(allow_sugar_cane_place_on.get(below));
            if (verbose)
                getLogger().info(String.format("blockCanBuild(%s:%d,%d,%d) SUGAR_CANE on %s %s", b.getWorld().getName(), b.getX(), b.getY(), b.getZ(), Material.getMaterial(below).name(), event.isBuildable()?"allowed":"cancelled"));
        }
    }
}

